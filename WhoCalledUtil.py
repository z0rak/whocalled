import os
import re
import json
from shutil import copyfile
from threading import Thread
from functools import partial
import sublime
import sublime_plugin

class indexer(sublime_plugin.EventListener):
  """
  Indexer class.
  """

  def __init__(self):
    """
    Python init method.
    """
    # String "constants".
    self.INDEX_TYPE_DEFINITION = "function definitions"
    self.INDEX_TYPE_CALL = "function calls"
    # Running Sublime Text 2?
    self.is_st2 = int(sublime.version()) < 3000
    # Define helper classes.
    self.util = util()

  def on_post_save(self, view):
    """
    Sublime Text callback after saving a file.
    """
    # Get paths.
    paths = self.util.get_paths()
    # Get settings.
    settings = self.util.get_settings()

    # Get current file and folder.
    file = view.file_name()
    folder = self.util.get_current_project_folder(True)
    folder_name = os.path.basename(folder)

    if settings['always_index_all_folders']:
      project_folders = self.util.get_all_project_folders()
    else:
      project_folders = self.util.get_project_folders()

    if folder in project_folders:
      # Create paths if necessary.
      if not os.path.isdir(paths['user_data_index']):
        os.makedirs(paths['user_data_index'])
      # Get supported language syntaxes.
      syntaxes = settings['syntaxes']
      # Run function on separate thread.
      Thread(target = self._on_post_save_run, args = (file, folder_name, syntaxes, paths)).start()

  def _on_post_save_run(self, file, folder_name, syntaxes, paths):
    """
    Starts indexer when a file is saved.
    """
    # Re-index current file.
    self._re_index_file(file, folder_name, syntaxes, paths)

  def re_index_files(self, folder):
    """
    Prepares re-indexing of files.
    """
    # Get paths.
    paths = self.util.get_paths()
    # Get settings.
    settings = self.util.get_settings()

    # Get supported language syntaxes.
    syntaxes = settings['syntaxes']
    # Get files to index.
    files_to_index = self._get_files_to_index(folder, syntaxes)
    # Get folder name.
    folder_name = os.path.basename(folder)

    for file in files_to_index:
      self._re_index_file(file, folder_name, syntaxes, paths)

  def index_files(self, folder):
    """
    Indexes by updating all files.
    """
    # Get paths.
    paths = self.util.get_paths()
    # Get settings.
    settings = self.util.get_settings()

    # Get supported language syntaxes.
    syntaxes = settings['syntaxes']

    # Get files to index.
    folder_name = os.path.basename(folder)
    files_to_index = self._get_files_to_index(folder, syntaxes, True)

    # Get maps of functions in folder.
    functions_defs_in_folder = self._search_files_for_functions(files_to_index, syntaxes, folder_name, self.INDEX_TYPE_DEFINITION)
    functions_calls_in_folder = self._search_files_for_functions(files_to_index, syntaxes, folder_name, self.INDEX_TYPE_CALL, functions_defs_in_folder[1])

    # Write JSON objects to files.
    with open(os.path.join(paths['user_data_index'], 'def_' + folder_name.replace(' ', '_') + '.json'), 'w') as map_file:
      map_file.write(json.dumps(functions_defs_in_folder[0]))

    with open(os.path.join(paths['user_data_index'], 'def_coll_' + folder_name.replace(' ', '_') + '.json'), 'w') as map_file:
      map_file.write(json.dumps(functions_defs_in_folder[1]))

    with open(os.path.join(paths['user_data_index'], 'call_' + folder_name.replace(' ', '_') + '.json'), 'w') as map_file:
      map_file.write(json.dumps(functions_calls_in_folder[0]))

  def _re_index_file(self, file, folder_name, syntaxes, paths):
    """
    Re-indexes by updating only the changed files.
    """
    # Get current file extension.
    file_extension = os.path.splitext(file)[1][1:]

    # Only for the allowed file extensions.
    if file_extension in syntaxes:
      # Get existing funtion map files.
      function_defs_map_file = os.path.join(paths['user_data_index'], 'def_' + folder_name.replace(' ', '_') + '.json')
      function_defs_coll_map_file = os.path.join(paths['user_data_index'], 'def_coll_' + folder_name.replace(' ', '_') + '.json')
      function_calls_map_file = os.path.join(paths['user_data_index'], 'call_' + folder_name.replace(' ', '_') + '.json')

      # Create buckets for function maps.
      function_defs_map_object = {}
      function_defs_coll_map_object = []
      function_calls_map_object = {}

      # Load maps of functions from file.
      if os.path.exists(function_defs_map_file):
        index = self.util.parse_json_file(function_defs_map_file)
        if isinstance(index, dict):
          function_defs_map_object = index

      if os.path.exists(function_defs_coll_map_file):
        index = self.util.parse_json_file(function_defs_coll_map_file)
        if isinstance(index, dict):
          function_defs_coll_map_object = index

      if os.path.exists(function_calls_map_file):
        index = self.util.parse_json_file(function_calls_map_file)
        if isinstance(index, dict):
          function_calls_map_object = index

      # Get map of functions definitions in file and update existing map (if any).
      function_defs_in_file = self._search_files_for_functions([file], syntaxes, folder_name, self.INDEX_TYPE_DEFINITION)
      function_defs_map_object.setdefault(file, {})
      if file in function_defs_in_file[0]:
        function_defs_map_object[file] = function_defs_in_file[0][file]

      # Append to collection of function definitions.
      function_defs_coll_map_object = function_defs_coll_map_object + list(set(function_defs_in_file[1]) - set(function_defs_coll_map_object))

      # Get map of functions calls in file and update existing map (if any).
      function_calls_in_file = self._search_files_for_functions([file], syntaxes, folder_name, self.INDEX_TYPE_CALL, function_defs_coll_map_object)
      function_calls_map_object.setdefault(file, {})
      if file in function_calls_in_file[0]:
        function_calls_map_object[file] = function_calls_in_file[0][file]

      # Write JSON object to files.
      with open(function_defs_map_file, 'w') as map_file:
        map_file.write(json.dumps(function_defs_map_object))

      with open(function_defs_coll_map_file, 'w') as map_file:
        map_file.write(json.dumps(function_defs_coll_map_object))

      with open(function_calls_map_file, 'w') as map_file:
        map_file.write(json.dumps(function_calls_map_object))

  def _search_files_for_functions(self, files_to_index, syntaxes, folder_name, index_type, functions = None):
    """
    Searches files for function definitions and calls.
    """
    # Create buckets for functions.
    function_map = {}
    function_index = []

    # Progress dots.
    max_no_of_dots = 50
    dot_counter = max_no_of_dots
    no_of_files = len(files_to_index)
    dot_interval = no_of_files / max_no_of_dots

    # Variables to use when displaying progress message.
    self.dots = ''
    self.no_of_files = no_of_files
    self.indexing_progress_message = 'Indexing ' + index_type + ' in folder ' + folder_name

    for file in files_to_index:
      file_extension = os.path.splitext(file)[1][1:]
      syntax = syntaxes[file_extension]

      if not None == functions:
        # Compile search for the function name and one parenthesis, without function definition keyword, followed by one space.
        regex = re.compile(syntax['call'])
      else:
        # Compile search for function definition keyword, followed by one space, the function name and one parenthesis.
        regex = re.compile(syntax['def'])

      try:
        # Read each file, but ignore any errors (typically caused by non-supported characters, but these are not allowed in functions anyway).
        with open(file, 'r') as opened_file:

          # Set default value if not present.
          function_map.setdefault(file, {})

          # Search!
          matches = regex.finditer(opened_file.read())

          # Grab new functions as long as there are any on the same line.
          for match in matches:
            function_name = match.group()

            buffer_position = match.start()
            # Add match to list.
            if not None == functions:
              if function_name in functions:
                # Set default value if not present and add line number.
                function_map[file].setdefault(function_name, []).append(str(buffer_position))
            else:
              # Set default value if not present and add line number.
              function_map[file].setdefault(function_name, []).append(str(buffer_position))
              if not function_name in function_index:
                function_index.append(function_name)

          # Remove empty indexes.
          if not function_map[file]:
            del function_map[file]

      except (UnicodeDecodeError, Exception) as e:
        # Do nothing.
        pass

      # Update progress dot threshold.
      dot_threshold = dot_counter * dot_interval

      # Show one dot for each passed threshold level.
      if dot_threshold > no_of_files:
        dot_counter = dot_counter - 1
        self._indexing_progress_message()
      no_of_files = no_of_files - 1

    # Set function index.
    if not None == functions:
      function_index = functions

    return (function_map, function_index)

  def _indexing_progress_message(self):
    """
    Progress message.
    """
    # Add dots.
    self.dots = self.dots + '.'
    # Set a minimum number of dots.
    if self.no_of_files < 150:
      self.dots = '...'

    self.util.error_message('WhoCalled says: "' + self.indexing_progress_message +  ' ' + self.dots + ' "')

  def _get_files_to_index(self, folder, syntaxes, force_indexing = False):
    """
    Walk through directories to look for files to index.
    """
    # Get paths.
    paths = self.util.get_paths()

    # Get timestamp of last indexing.
    last_indexing_timestamp_file = os.path.join(paths['user_data'], 'last_indexing_timestamp')
    last_crawl = os.path.getmtime(last_indexing_timestamp_file)

    files_to_index = set()
    try:
      # Traverse each sub folder in project folder.
      for current, sub, files in os.walk(folder):
        # Traverse each file in each sub folder.
        for file in files:
          current_file = os.path.join(current, file)
          # Only index file if it has changed since last indexing.
          if force_indexing or (os.path.getmtime(current_file) > last_crawl):
            file_extension = os.path.splitext(current_file)[1][1:]
            # Only allowed syntaxes.
            if file_extension in syntaxes:
              files_to_index.add(current_file)

    except (FileNotFoundError, Exception) as e:
      # Do nothing.
      pass

    return files_to_index

class util(object):
  """
  Util class.
  """

  def __init__(self):
    """
    Python init method.
    """
    # Running Sublime Text 2?
    self.is_st2 = int(sublime.version()) < 3000

  def do_when(self, conditional, callback, *args, **kwargs):
    """
    Runs every 50th millisecond and executes when condition is true.
    Note: Graciously borrowed from a Sublime Text 2 git plugin by David Lynch,
    aka "kemayo" (https://github.com/kemayo/sublime-text-2-git).
    """
    if conditional():
      return callback(*args, **kwargs)
    sublime.set_timeout(partial(self.do_when, conditional, callback, *args, **kwargs), 50)

  def get_all_project_folders(self):
    """
    Fetches all project folders.
    """
    # Get window and view.
    window = sublime.active_window()
    view = window.active_view()

    # Get project folders.
    project_folders = window.folders()

    return project_folders

  def get_project_folders(self):
    """
    Fetches active project folders.
    """
    # Get paths.
    paths = self.get_paths()
    # Get settings.
    settings = self.get_settings()

    # Create paths if necessary.
    if not os.path.isdir(paths['user_data_index']):
      os.makedirs(paths['user_data_index'])

    # Get project folders.
    if settings['always_index_all_folders']:
      project_folders = self.get_all_project_folders()
    else:
      project_folders_path = os.path.join(paths['user_data'], 'index_folders.json')
      if not os.path.exists(project_folders_path):
        with open(project_folders_path, 'w') as file:
          file.write(json.dumps([]))

      project_folders = self.parse_json_file(project_folders_path)
      if not isinstance(project_folders, list):
        self.error_message('WhoCalled says: "Could not get project folders."')

    return project_folders

  def get_current_project_folder(self, whole_path = False):
    """
    Fetches project folder for the file currently being edited.
    """
    # Set window and view.
    window = sublime.active_window()
    view = window.active_view()
    # Get project folders.
    project_folders = window.folders()
    # Get path to current file.
    file_name = view.file_name()
    path = os.path.dirname(file_name)

    # Get project folder for current file.
    folder_name = ''
    for folder in project_folders:
      if folder in path:
        if not whole_path:
          folder_name = os.path.basename(folder)
        folder_name = folder
        break

    return folder_name

  def parse_json_file(self, file, clean_json = False):
    """
    Parses a JSON file and removes invalid patterns.
    """
    try:
      if clean_json:
        # Regular expression for comments.
        comment_re = re.compile(
          '(^)?[^\S\n]*/(?:\*(.*?)\*/[^\S\n]*|/[^\n]*)($)?',
          re.DOTALL | re.MULTILINE
        )

        with open(file, 'r') as f:
          content = ''.join(f.readlines())
          # Remove illegal commas (,) from last item in JSON arrays.
          content = re.sub(r',\n\s+]', '\n]', content)
          # Look for comments.
          match = comment_re.search(content)
          while match:
            content = content[:match.start()] + content[match.end():]
            match = comment_re.search(content)

        json_object = json.loads(content)

      else:
        with open(file, 'r') as f:
          content = ''.join(f.readlines())
          json_object = json.loads(content)

      return json_object

    except (ValueError, Exception) as e:
      self.error_message('WhoCalled says: "We have a JSON error. Check the \"Traceback\" in your console and tell the developer."')

  def error_message(self, message):
    """
    Prints an error message in the status bar.
    """
    sublime.set_timeout(lambda: sublime.status_message(message), 1)

  def get_paths(self):
    """
    Fetches important paths.
    """
    # Get project name.
    if self.is_st2:
      project_name = '_default_'
    else:
      window = sublime.active_window()
      project_file_name = window.project_file_name()
      if project_file_name:
        project_name = os.path.basename(project_file_name).replace('.sublime-projects.sublime-project', '')
      else:
        project_name = '_default_'

    # Create paths object.
    sublime_packages_path = sublime.packages_path()
    paths = {
      'user': os.path.join(sublime_packages_path, 'User'),
      'user_data': os.path.join(sublime_packages_path, 'User', 'WhoCalledData', project_name),
      'user_data_index': os.path.join(sublime_packages_path, 'User', 'WhoCalledData', project_name, 'index')
    }
    return paths

  def get_settings(self):
    """
    Fetches settings. Does it differently depending on the version of Sublime Text.
    """
    if self.is_st2:
      settings = self.get_settings_st2()
    else:
      settings = self.get_settings_st3()

    return settings

  def get_settings_st2(self):
    """
    Fetches settings from user or default settings file (in that order). Only for SublimeT2.
    """
    # Get paths.
    paths = self.get_paths()

    # Copy default settings file to user settings if no user settings file is present.
    user_settings_path = os.path.join(paths['user'], 'WhoCalled.sublime-settings')
    default_settings_path = os.path.join(sublime.packages_path(), 'WhoCalled Function Finder', 'WhoCalled.sublime-settings')
    if not os.path.exists(user_settings_path):
      copyfile(default_settings_path, user_settings_path)

    # Load and settings files.
    default_settings_object = self.parse_json_file(default_settings_path, True)
    if not isinstance(default_settings_object, dict):
      self.error_message('WhoCalled says: "Could not load default settings."')
    user_settings_object = self.parse_json_file(user_settings_path, True)
    if not isinstance(user_settings_object, dict):
      self.error_message('WhoCalled says: "Could not load user settings."')

    # Get settings.
    default_settings = default_settings_object['settings']
    user_settings = user_settings_object['settings']

    # Set user/default settings.
    settings = {}
    settings['always_force_indexing'] = user_settings['always_force_indexing'] if 'always_force_indexing' in user_settings else default_settings['always_force_indexing']
    settings['always_index_all_folders'] = user_settings['always_index_all_folders'] if 'always_index_all_folders' in user_settings else default_settings['always_index_all_folders']
    settings['syntaxes'] = user_settings_object['syntaxes'] if 'syntaxes' in user_settings_object else default_settings_object['syntaxes']

    return settings

  def get_settings_st3(self):
    """
    Fetches settings. Only for ST3.
    """
    # Get paths.
    paths = self.get_paths()

    # Load settings file.
    settings_object = sublime.load_settings('WhoCalled.sublime-settings')

    # Get settings and sytaxes.
    settings = settings_object.get('settings')
    syntaxes = settings_object.get('syntaxes')

    # Add syntaxes to settings.
    settings['syntaxes'] = syntaxes

    return settings